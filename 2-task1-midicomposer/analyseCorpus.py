# FIXME This is currently not usable since it lacks the input files. Those could be generated again by writing the
# intermediate csv to a file again or using the csv as input directly.

file = open('./data/midi_in/Bach_French1.txt', "r")  # Input
lines = file.readlines()
file.close()
histo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
distances = []
lengths = []
notes = []
for l in lines:
    s = l.split(";")
    histo[int((s.__len__() - 1) / 2)] += 1
    if not distances.__contains__(int(s[0])):
        distances.append(int(s[0]))
    for i in range(s.__len__()):
        if i % 2 == 1:
            if not notes.__contains__(int(s[i])):
                notes.append(int(s[i]))
        elif i > 0:
            if not lengths.__contains__(int(s[i])):
                lengths.append(int(s[i]))
for i in range(histo.__len__()):
    print('Laenge ' + str(i) + ": " + str(histo[i]))

distances.sort()
lengths.sort()
notes.sort()

out = "Distanzen: "
for l in distances:
    out += (str(l) + ";")
print(out)
print("Es gibt " + str(distances.__len__()) + " moegliche Distanzen")

out = "Laengen: "
for l in lengths:
    out += str(l) + ";"

print(out)
print("Es gibt " + str(lengths.__len__()) + " moegliche Tonlaengen")

out = "Tonhoehen: "
for l in notes:
    out += str(l) + ";"

print(out)
print("Es gibt " + str(notes.__len__()) + " moegliche Tonhoehen")
