# Task 1: Autogenerate music in the midi format by training an LSTM Model

## Setup
- Start by setting up virtualenv (PyCharm can do this for you - search for interpreter in the settings, and create a  new Virtualenv environment).
- Run `pip install -r requirements.txt` or let PyCharm do this for you.
- You are all setup!

## Structure
The Midi Project is located in the 2-task1-midicomposer folder

### Conversion

Conversion between .mid and our .lstm file format is handled by lstm_conversion.py and midi_lstm.py. Midi files are brought into a readeable format using the py_midicsv library.
During conversion to .lstm we use header information to preserve the relative speed between different tracks.
.lstm holds information about the time passed between notes played, aswell as pitch and the duration a note is held. Notes started at the same time are saved in a single line, allowing up to 4 notes played in parallel.
When predicting new music we use our default header (default_header.csv).

### Data 

All of our data is compiled in the data folder. Lstm_in contains the corpus used for training. Our latest results have been predicted using bach_korpus_modified.lstm.
This corpus has been created using our conversion methods on the midi files located in midi_in.
When generating new music a lstm-version is created in lstm_out aswell as a midi file in midi_out.
Models are saved in the models folder. A model trained for 10h is provided.

### Training and Prediction

model_input.py contains our helper functions surrounding training and prediction.
create_x_y_data() automatically creates the training and label data for our model. You can choose how many past notes are taken into account by specifying the sequence length. A boolean mask can be used to decides which parts/channels of the lstm lines are considered for training. 
The input is converted to mult-hot vectors, containing a 1-hot vector for each used channel. 
In the last week we added a post processing (post_proc_lstm) step to the corpus where we limit the possible note/event lenghts to 1, 1/2 , 1/4.. 1/32 notes drastically reducing the length of the hot vectors.
Predictions are made with the predict_music function. Output length aswell as strategies for replacing non-predicted channels can be choosen.
For each channel the output of the model is normalized and then a note is guessed by the resulting probability distribution.

Our model and control of training and prediction can be found in model.py. This is the master file 
you can use to create new models and predict new music. By default training is turned of by an if statement, which can be reenabled by setting training=True.

## Playing a .mid file

- Windows media player should support .mid files
- if you use a different media player, e.g. VLC, you will need to provide a Soundfont. A Tutorial can be found [here](https://www.vlc-forum.de/thread/1121-midi-dateien-mit-dem-vlc-player-wiedergeben/)


## Corpus 

Our training data is a subset of the midi corpus found on [this reddit post](https://www.reddit.com/r/WeAreTheMusicMakers/comments/3ajwe4/the_largest_midi_collection_on_the_internet/)


## Workflow

An example workflow and the usage of the scripts can be found in `example_workflow.py`.

- The training corpus in midi format can be found in `data/midi_in`.
- The midi files are converted to vectors for the LSTM net and saved in `/data/lstm_in`.
- The LSTM model should be trained on these data.
- The LSTM model should predict new music starting from a given note.
- This output should be saved in `data/lstm_out`.
- It can be converted back to midi and saved in `data/midi_out`. A default header is added automatically with the current conversion script.
