import os

import py_midicsv

from .lstm_conversion import csv_to_lstm, lstm_to_csv


def midi_files_to_lstm(midi_directory_name, lstm_file_name):
    print("Starting conversion of midi files in directory %s ..." % midi_directory_name)

    line_count = 0
    lstm_out_file = open(lstm_file_name, 'w+')

    for file_name in os.listdir(midi_directory_name):
        if not file_name.endswith('.mid'):
            continue

        full_file_name = midi_directory_name + '/' + file_name
        csv = _midi_file_to_csv(full_file_name)
        csv_lines = csv.split('\n')
        lstm_lines = csv_to_lstm(csv_lines)

        for l in lstm_lines:
            lstm_out_file.write(l)
        line_count += len(lstm_lines)
        lstm_out_file.write("-1,-1,-1,-1,-1,-1,-1,-1,-1\n")
    lstm_out_file.close()
    print("Wrote a total of %i lines to file %s" % (line_count, lstm_file_name))


def lstm_to_midi_file(lstm_file_name, midi_file_name):
    print("Starting conversion of lstm file %s to midi file %s ..." % (lstm_file_name, midi_file_name))
    with open(lstm_file_name, "r") as lstm_file:
        lstm_lines = lstm_file.readlines()

        csv_lines = _get_header_lines()
        csv_lines.extend(lstm_to_csv(lstm_lines))

        pattern = py_midicsv.csv_to_midi(csv_lines)
        with open(midi_file_name, "wb") as midi_file:
            midi_writer = py_midicsv.FileWriter(midi_file)
            midi_writer.write(pattern)

    print("Done.")


def _midi_file_to_csv(in_file_name):
    with open(in_file_name, "rb") as input_file:
        converted = py_midicsv.midi_to_csv(input_file)
        return converted.getvalue()


def _get_header_lines():
    with open("./conversion/default_header.csv", "r") as header_file:
        return header_file.readlines()
