import re

LSTM_LINE_SEPARATOR = ","


class NoteEvent:
    def __init__(self, time, note, state):
        self.time = time
        self.note = note
        self.state = state


class PlayEvent:
    def __init__(self, distance):
        self.distance = distance
        self.notes = []
        self.durations = []


class MidiLine:
    def __init__(self, time, string):
        self.time = time
        self.string = string


def csv_to_lstm(csv_lines):
    note_lines = _get_notes(csv_lines)
    event_list = _get_events(note_lines)

    return _convert_to_lstm_lines(event_list)


def lstm_to_csv(lstm_lines):
    metronome = 120
    tempo_multiplier = metronome / 96

    midi_lines = []
    current_time = 0
    for l in lstm_lines:  # Feed in Event data
        p = l.split(LSTM_LINE_SEPARATOR)
        notes = []
        durations = []

        current_time += int(int(p[0]) * tempo_multiplier)
        for i in range(1, 8, 2):
            if not int(p[i]) == 0:
                notes.append(int(p[i]))
                durations.append(int(int(p[i + 1]) * tempo_multiplier))
        while durations.__len__() > 0:  # Create note starting and ending lines
            note = notes.pop(0)
            duration = durations.pop(0)
            midi_lines.append(MidiLine(current_time, "2, %s, Note_on_c, 0, %s, 100" % (str(current_time), str(note))))
            midi_lines.append(MidiLine(current_time + duration, "2, %s, Note_on_c, 0, %s, 0" % (str(current_time + duration), str(note))))

    # Sort lines to get sequential order
    midi_lines.sort(key=lambda midi_line: midi_line.time)

    csv_lines = map(lambda midi_line: midi_line.string, midi_lines)
    return csv_lines


def _get_notes(csv_lines):
    """
    This creates a list of NoteEvents ordered by time from a list of comma separated lines.
    :param csv_lines:
    :return:
    """

    tempo = 0
    metronome = 0
    note_lines = []
    for csv_line in csv_lines:
        # Find metronome
        if re.search('Header', csv_line):
            header_line = csv_line.split(",")
            metronome = int(header_line[5])

        # Find tempo
        if re.search('Tempo', csv_line):
            tempo_line = csv_line.split(",")
            if int(tempo_line[3]) < 300000:
                tempo = 4
            elif int(tempo_line[3]) < 500000:
                tempo = 2
            elif int(tempo_line[3]) < 700000:
                tempo = 1.5
            else:
                tempo = 1

        # Convert actual notes
        if re.search('Note_', csv_line):
            # save playtime frequency and whether it is starting or ending a note (might need tuning for files using
            # the Note_Off statement)
            note_line = csv_line.split(",")
            time = int(int(note_line[1]) / metronome * (96 / tempo))
            note = int(note_line[4])
            state = int(note_line[5]) >= 100
            note_lines.append(NoteEvent(time, note, state))

    # sort by playtime to combine multiple tracks into one ordered track
    note_lines.sort(key=lambda line: line.time)
    return note_lines


def _get_events(note_lines):
    """
    Reformat the data into events that hold information about all notes being played at the same time and the
    duration they are held for.
    :param note_lines:
    :return:
    """

    event_list = []
    last_event_start = 0
    while note_lines.__len__() > 0:
        cur_lines = [note_lines.pop(0)]
        note_lines_copy = note_lines.copy()  # work on copy array to allow safe deletion during for loop
        for l in note_lines:  # compile all lines that start a note at the same timestamp
            if l.time == cur_lines[0].time and l.state:
                cur_lines.append(l)
                note_lines_copy.remove(l)
            else:
                break
        note_lines = note_lines_copy
        event = PlayEvent(
            cur_lines[0].time - last_event_start)  # initialize new event with time distance to previous event
        last_event_start = cur_lines[0].time
        notes = []
        durations = []
        while cur_lines.__len__() > 0:  # match and remove all note ending lines corresponding to the new event
            find_match = cur_lines.pop(0)
            notes.append(find_match.note)
            for l in note_lines:
                if find_match.note == l.note and not l.state:
                    durations.append(l.time - find_match.time)
                    note_lines.remove(l)
                    break
        event.notes = notes
        event.durations = durations
        event_list.append(event)

    return event_list


def _convert_to_lstm_lines(event_list):
    """
    Each event will correspond to one line in the output. Output format: Distance_to_previous_event/Note1/Duration_of_Note1/Note2/...
    :param event_list:
    :return:
    """

    out_lines = []
    for event in event_list:
        csv_line = str(event.distance)
        for i in range(4):
            if i < event.notes.__len__():
                csv_line += LSTM_LINE_SEPARATOR + str(event.notes[i]) + LSTM_LINE_SEPARATOR + str(event.durations[i])
            else:
                csv_line += LSTM_LINE_SEPARATOR + "0" + LSTM_LINE_SEPARATOR + "0"
        csv_line += "\n"
        out_lines.append(csv_line)
    return out_lines
