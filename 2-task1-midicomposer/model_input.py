from conversion.midi_lstm import lstm_to_midi_file
import pandas as pd
import numpy as np


def create_x_y_data(filename, used_channels, sequence_length):
    all_data = np.array(pd.read_csv(filename))
    num_used_channels = 0
    for i in used_channels:
        if i:
            num_used_channels += 1
    data = np.empty(shape=(all_data.shape[0], num_used_channels), dtype=int)

    for i in range(len(all_data)):
        data[i] = all_data[i][used_channels]

    song_separated = []
    song = []
    for i in range(data.shape[0]):

        if data[i, 0] == -1:
            song_separated.append(song)
            song = []
        else:
            song.append(data[i].tolist())

    print("Slicing input into Songs. %d slices created!" % len(song_separated))
    print("Creating input sequences of length %d using %d channels..." % (sequence_length, len(used_channels)))

    corpus = np.array(song_separated[0])
    for i in range(1,song_separated.__len__()):
        corpus = np.append(corpus, np.array(song_separated[i]))

    corpus = corpus.reshape(-1, num_used_channels)
    encoding_tables = get_encoding_tables(corpus)
    x_data = []
    y_data = []

    for i in range(len(song_separated)):
        for j in range(len(song_separated[i]) - sequence_length):
            x_data.append(song_separated[i][j:j + sequence_length])
            y_data.append(song_separated[i][j + sequence_length])

    x_data = np.array(x_data)
    y_data = np.array(y_data)
    x_data = x_data.reshape(-1, num_used_channels)
    print("Encoding target labels")
    y_hot_encoded = get_multi_hot_encoding(encoding_tables, y_data)
    print("Encoding training data - this might take a while")
    x_hot_encoded = get_multi_hot_encoding(encoding_tables, x_data)

    x_hot_encoded = x_hot_encoded.reshape(-1, sequence_length, y_hot_encoded.shape[1])
    return x_hot_encoded, y_hot_encoded, encoding_tables


def get_multi_hot_encoding(encoding_tables, data):
    multi_vec_len = 0
    for i in range(len(encoding_tables)):
        multi_vec_len += len(encoding_tables[i])
    print("Encoding data as %d_hot vectors with lenght... %d" % (len(encoding_tables), multi_vec_len))

    output = np.zeros(shape=(data.shape[0], multi_vec_len), dtype=int)
    for i in range(data.shape[0]):
        hot_vec = np.empty(shape=0, dtype=int)
        for j in range(data.shape[1]):
            hot_vec_j = np.zeros(shape=len(encoding_tables[j]))
            hot_vec_j[encoding_tables[j].index(data[i, j])] = 1
            hot_vec = np.append(hot_vec, [hot_vec_j])
        output[i] = hot_vec

    return output


def get_encoding_tables(data):
    print("Calculating encoding tables...")
    encoding_tables = []
    for i in range(data.shape[1]):
        table = []
        for j in range(data.shape[0]):
            if data[j, i] not in table:
                table.append(data[j, i])
        encoding_tables.append(table)
    return encoding_tables


def decode_mult_hot_vector(vector, decoding_tables, mask):
    default_values = [24, 74, 24, 0, 0, 0, 0, 0, 0]
    channels = np.where(mask)
    lstm_line = np.zeros(9, dtype=int)
    split_vector = []
    k = 0
    for i in range(len(channels[0])):
        split_vector.append(vector[k:len(decoding_tables[i])+k].tolist())
        k += len(decoding_tables[i])
    j = 0
    for i in range(len(default_values)):
        if mask[i]:
            norm = np.sum(split_vector[j])
            split_vector[j] = split_vector[j]/norm
            lstm_line[i] = np.random.choice(decoding_tables[j], 1, p=split_vector[j])
            j += 1
        else:
            lstm_line[i] = default_values[i]

    return lstm_line


def predict_music(model, decoding_tables, mask, output_len, filename, input_sequence=None, replacement_speed=-1):

    print("Creating music prediction of length %d" % output_len)
    input_shape = (model.layers[0].input_shape[1], model.layers[0].input_shape[2])
    hot_encoding_len = model.layers[-1].output_shape[1]
    music = np.empty(shape=(output_len, len(mask)), dtype=int)
    if input_sequence is None:
        print("Initializing random input sequence...")
        input_sequence = np.empty(shape=(input_shape[0], input_shape[1]), dtype=int)
        for i in range(input_sequence.shape[0]):
            input_sequence[i] = decode_mult_hot_vector(np.random.rand(hot_encoding_len), decoding_tables, mask)[mask]
    if not input_sequence.shape == input_shape:
        print("Wrong input_sequence shape! Expected shape was %s but got array with shape %s" % (input_shape, input_sequence.shape))
        return
    input_sequence = input_sequence.reshape(1, input_shape[0], input_shape[1])
    for i in range(output_len):
        prediction = decode_mult_hot_vector(model.predict(input_sequence)[0], decoding_tables, mask)
        music[i] = prediction
        input_sequence = np.append(input_sequence, prediction[mask])
        input_sequence = input_sequence[input_shape[1]:]
        input_sequence = input_sequence.reshape(1, input_shape[0], input_shape[1])

    if not replacement_speed == -1:
        print("Replacing all unpredicted duration information with %d" % replacement_speed)
        for k in range(output_len):
            for i in range(0,len(mask),2):
                if not mask[i]:
                    music[k][i] = replacement_speed
    else:
        if not mask[0]:
            print("Your model doesn't predict note timings. You need to set a replacement speed!!!")
            return
        print("Replacing all unpredicted duration information with corresponding note timing")
        for k in range(output_len):
            for i in range(2,len(mask),2):
                if not mask[i]:
                    music[k][i] = music[k][0]
    print("Creating .LSTM file at ./data/lstm_out/%s.lstm" % filename)
    lstm_file = open("./data/lstm_out/"+filename+".lstm", "w+")
    for i in range(output_len):
        string = ""
        for j in range(len(mask)):
            string += str(music[i, j])
            if j < len(mask)-1:
                string += ","

        string += "\n"
        lstm_file.writelines(string)
    lstm_file.close()
    lstm_to_midi_file("./data/lstm_out/"+filename+".lstm", "./data/midi_out/"+filename+".mid")


def predict_music_hot_input(model, decoding_tables, mask, output_len, filename, input_sequence=None, replacement_speed=-1):

    print("Creating music prediction of length %d" % output_len)
    input_shape = (model.layers[0].input_shape[1], model.layers[0].input_shape[2])

    music = np.empty(shape=(output_len, len(mask)), dtype=int)
    if input_sequence is None:
        print("Initializing random input sequence...")
        input_sequence = np.empty(shape=(input_shape[0], input_shape[1]), dtype=int)
        for i in range(input_sequence.shape[0]):
            inp_line = np.zeros(input_shape[1])
            inp_line[np.random.randint(0, input_shape[1]-1)] = 1
            input_sequence[i] = inp_line
    if not input_sequence.shape == input_shape:
        print("Wrong input_sequence shape! Expected shape was %s but got array with shape %s" % (
        input_shape, input_sequence.shape))
        return
    input_sequence = input_sequence.reshape(1, input_shape[0], input_shape[1])
    for i in range(output_len):
        prediction = model.predict(input_sequence)[0]
        music[i] = decode_mult_hot_vector(prediction, decoding_tables, mask)
        input_sequence = np.append(input_sequence, prediction)
        input_sequence = input_sequence[input_shape[1]:]
        input_sequence = input_sequence.reshape(1, input_shape[0], input_shape[1])

    if not replacement_speed == -1:
        print("Replacing all unpredicted duration information with %d" % replacement_speed)
        for k in range(output_len):
            for i in range(0, len(mask), 2):
                if not mask[i]:
                    music[k][i] = replacement_speed
    else:
        if not mask[0]:
            print("Your model doesn't predict note timings. You need to set a replacement speed!!!")
            return
        print("Replacing all unpredicted duration information with corresponding note timing")
        for k in range(output_len):
            for i in range(2, len(mask), 2):
                if not mask[i]:
                    music[k][i] = music[k][0]
    print(music)
    print("Creating .LSTM file at ./data/lstm_out/%s.lstm" % filename)
    lstm_file = open("./data/lstm_out/" + filename + ".lstm", "w+")
    for i in range(output_len):
        string = ""
        for j in range(len(mask)):
            string += str(music[i, j])
            if j < len(mask) - 1:
                string += ","

        string += "\n"
        lstm_file.writelines(string)
    lstm_file.close()
    lstm_to_midi_file("./data/lstm_out/" + filename + ".lstm", "./data/midi_out/" + filename + ".mid")


def post_proc_lstm(infile, outfile):
    lstm_file = open("./data/lstm_in/" + infile + ".lstm", "r")
    pre_lstm = np.array(pd.read_csv(lstm_file))
    for l in pre_lstm:
        if l[0] == -1:
            continue
        for i in range(0,len(l),2):
            if l[i] < 6:
                l[i] = 0
            elif l[i] < 18:
                l[i] = 12
            elif l[i] < 36:
                l[i] = 24
            elif l[i] < 72:
                l[i] = 48
            elif l[i] < 144:
                l[i] = 96
            else:
                l[i] = 192
    out_len = 0
    for l in pre_lstm:
        if l[0] != 0:
            out_len +=1
    post_lstm = np.empty(shape=(out_len, pre_lstm.shape[1]), dtype=int)
    k = 0
    for i in range(len(pre_lstm)):
        j = 0
        if pre_lstm[i][0] != 0:
            post_lstm[k] = pre_lstm[i]
            k += 1
            j = 0
        elif j < 3:
            post_lstm[k-1][3+(2*j)] = pre_lstm[i][1]
            post_lstm[k-1][4 + (2 * j)] = pre_lstm[i][2]
            j += 1
    out_lstm = open("./data/lstm_in/" + outfile + ".lstm", "w+")
    for i in range(len(post_lstm)):
        string = ""
        for j in range(len(post_lstm[0])):
            string += str(post_lstm[i , j])
            if j < len(post_lstm[0]) - 1:
                string += ","

        string += "\n"
        out_lstm.writelines(string)
    out_lstm.close()
