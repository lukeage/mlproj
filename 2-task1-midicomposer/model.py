from model_input import create_x_y_data, decode_mult_hot_vector, predict_music , get_multi_hot_encoding, predict_music_hot_input
import tensorflow as tf
import keras
from keras import layers
from keras import models
import numpy as np
import time

filename = "data/lstm_in/bach_korpus_modified.lstm"
mask = np.ones(9, dtype=bool)
mask[[2, 3, 4, 5, 6, 7, 8]] = False

batch_size = 64
sequence_len = 32
train, label, enc_tables = create_x_y_data(filename, mask, sequence_len)
model = models.Sequential()
model.add(layers.LSTM(
        256,
        input_shape=(train.shape[1], train.shape[2]),
        return_sequences=True, dropout=0.1
    ))
model.add(layers.LSTM(256, dropout=0.1))
model.add(layers.Dense(label.shape[1], activation='sigmoid'))
# One-Hot -> softmax
model.compile(loss='binary_crossentropy', metrics=['accuracy'], optimizer='rmsprop')
# One-Hot -> categorical_crossentropy

'''
model.fit(train, label, epochs=1, batch_size=batch_size) #new model initialization
model.save('data/models/new_model.h5')
predict_music_hot_input(model, enc_tables, mask, 800, 'proveofconceptRand')
predict_music_hot_input(model, enc_tables, mask, 800, 'proveofconcept', input_sequence=train[0])
'''
training = False #skips training
if training:
    for i in range(1, 10):
        model = models.load_model('data/models/2hot_input_32seq_256_256_mod_korpus.h5')
        epochs = 0
        runtime = 3600
        start = time.time()
        while time.time()-start < runtime:
            print("Starting epoch %d" % epochs)
            print("Remaining Time: " + str(time.strftime('%H:%M:%S', time.gmtime(runtime-(time.time()-start)))))
            epochs += 1
            model.fit(train, label, epochs=1, batch_size=batch_size)

        model.save('data/models/2hot_input_32seq_256_256_mod_korpus.h5')

model = model = models.load_model('data/models/2hot_input_32seq_256_256_mod_korpus.h5')
filename = "my_midi_"  # create new track
predict_music_hot_input(model, enc_tables, mask, 800, filename, input_sequence=train[0])  # Bach input
filename += "_R"
predict_music_hot_input(model, enc_tables, mask, 800, filename)  # random input
