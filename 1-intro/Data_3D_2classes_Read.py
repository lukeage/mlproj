"""read and display data from csv at url
"""

# noinspection PyUnresolvedReferences
from mpl_toolkits import mplot3d

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf
from tensorflow import keras


def task1():
    csvname = './Data_3D_2classes.csv'

    print('Reading "' + csvname + '":')
    dat = np.loadtxt(csvname, delimiter=';')
    df = pd.DataFrame(dat)
    print(df.describe())

    fig = plt.figure()
    ax = plt.axes(projection='3d')

    for i, d in enumerate(dat):
        if d[3] > 0:
            ax.scatter(d[0], d[1], d[2], color='#0000FF', marker='o')
        else:
            ax.scatter(d[0], d[1], d[2], color='#FF8000', marker='o')

    # Fixed layout
    for spine in ax.spines.values():
        spine.set_visible(False)

    plt.title('Distribution (two classes)')
    ax.set_xlabel('Random points')
    ax.set_ylabel('Class 1 = Blue, Class -1 = Orange')
    ax.set_zlabel('Z Label')
    plt.show()

    train, trainlabel = dat[:, :3], dat[:, 3]
    print(train)
    model = keras.Sequential()  # new neural net
    model.add(keras.layers.Dense(3, activation='relu'))
    model.compile(optimizer=tf.train.AdamOptimizer(0.5),
                  loss='mse',  # mean squared error
                  metrics=['accuracy'])

    model.fit(train, trainlabel, epochs=10, batch_size=200,  # train and test
              validation_split=0.2)

    data, label = dat[:, :3], dat[:, 3]
    model = keras.Sequential()

    # Not working prediction...
    np_array = np.array([[1, 2, 2]])
    print(np_array)
    # print(model.predict_classes(train))

    # Not working different network

    # model.add(keras.layers.Dense(2, input_dim=3, activation='relu'))
    # # model.add(keras.layers.Dense(6, activation='relu'))
    # # model.add(keras.layers.Dense(6, activation='relu'))
    # model.add(keras.layers.Dense(1, activation='softmax'))
    # # ada = keras.optimizers.Adagrad(lr=0.2, epsilon=None, decay=0.01)
    # adam = keras.optimizers.Adam(lr=0.5, decay=0.001)
    # model.compile(loss='mse', optimizer=adam, metrics=['accuracy'])
    # model.fit(data, label, batch_size=5, epochs=200, validation_split=0.2)


if __name__ == '__main__':
    task1()
