package de.group5.evosnake;

import java.awt.*;
import java.util.Set;

import de.group5.evosnake.agent.HumanPlayer;
import de.group5.evosnake.game.Field;
import de.group5.evosnake.game.GameOverException;
import de.group5.evosnake.game.Position;
import de.group5.evosnake.game.SnakeController;
import javax.swing.*;

public class InteractivePlayUI {

	private static final long TICK_TIME = 100;

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = createAppFrame();
		frame.setVisible(true);

		HumanPlayer humanPlayer = new HumanPlayer();
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(humanPlayer);
		SnakeController snakeController = new SnakeController(humanPlayer);

		BoardUI boardUI = new BoardUI(snakeController.getBoard());
		boardUI.setSize(1000, 1000);
		frame.add(boardUI, BorderLayout.CENTER);

		mainLoop(snakeController, boardUI, frame);

	}

	private static void mainLoop(SnakeController snakeController, BoardUI boardUI, JFrame frame)
			throws InterruptedException
	{
		while (true) {
			Set<Position> changedPositions;
			try {
				changedPositions = snakeController.nextTick();
			} catch (GameOverException e) {
				System.out.println("Game over! Final length: " + e.getFinalScore());
				break;
			}
			Field[][] board = snakeController.getBoard();
			boardUI.paintNextTick(board, changedPositions);

			SwingUtilities.invokeLater(frame::repaint);
			Thread.sleep(TICK_TIME);
		}
	}

	private static JFrame createAppFrame() {
		JFrame frame = new JFrame("EvoSnake");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setBackground(Color.BLUE);

		BorderLayout manager = new BorderLayout();
		frame.setLayout(manager);
		return frame;
	}
}
