package de.group5.evosnake;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.group5.evosnake.agent.Agent;
import de.group5.evosnake.game.Field;
import de.group5.evosnake.game.GameOverException;
import de.group5.evosnake.game.Position;
import de.group5.evosnake.game.SnakeController;
import de.group5.evosnake.genetics.Generation;
import de.group5.evosnake.genetics.GeneticAlgorithmImpl;
import javax.swing.*;

public class BestAgentPlayUI {

	/**
	 * Time between game ticks in milliseconds
	 */
	private static final long TICK_TIME = 17;

	private static final String MODEL_FILENAME = "test";
	private static final long SEED = 1016;

	public static void main(String[] args) throws InterruptedException, IOException {
		JFrame frame = createAppFrame();
		frame.setVisible(true);

		java.util.List<SnakeController> snakeControllers = new ArrayList<>();
		java.util.List<BoardUI> boardUIs = new ArrayList<>();

		createBoard("best", frame, snakeControllers, boardUIs);
		createBoard("1000", frame, snakeControllers, boardUIs);
		createBoard("100", frame, snakeControllers, boardUIs);
		createBoard("5", frame, snakeControllers, boardUIs);

		frame.pack();
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

		mainLoop(snakeControllers, boardUIs, frame);

	}

	private static void createBoard(String generationSuffix, JFrame frame, List<SnakeController> snakeControllers, List<BoardUI> boardUIs)
			throws IOException
	{
		GeneticAlgorithmImpl geneticAlgorithm = new GeneticAlgorithmImpl();
		Generation lastGeneration = geneticAlgorithm.loadModel(MODEL_FILENAME + "_" + generationSuffix);
		Agent agent = lastGeneration.getBestAgent();

		SnakeController snakeController = new SnakeController(agent, SEED);
		BoardUI boardUI = new BoardUI(snakeController.getBoard());
		boardUI.setSize(450, 450);

		JPanel boardPanel = new JPanel();
		boardPanel.setLayout(new BorderLayout());
		boardPanel.add(new JLabel("Generation " + generationSuffix), BorderLayout.NORTH);
		boardPanel.add(boardUI, BorderLayout.CENTER);

		frame.add(boardPanel);

		boardUIs.add(boardUI);
		snakeControllers.add(snakeController);
	}

	private static void mainLoop(List<SnakeController> snakeControllers, List<BoardUI> boardUIs, JFrame frame)
			throws InterruptedException
	{
		boolean[] gameOver = new boolean[snakeControllers.size()];
		for (int i = 0; i < snakeControllers.size(); i++) {
			gameOver[i] = false;
		}

		while (true) {
			for (int i = 0; i < snakeControllers.size(); i++) {
				if (gameOver[i]) {
					continue;
				}
				SnakeController snakeController = snakeControllers.get(i);
				BoardUI boardUI = boardUIs.get(i);

				Set<Position> changedPositions;
				try {
					changedPositions = snakeController.nextTick();
				} catch (GameOverException e) {
					System.out.println("Game " + i + " over! Final length: " + e.getFinalScore());
					gameOver[i] = true;
					break;
				}
				Field[][] board = snakeController.getBoard();
				boardUI.paintNextTick(board, changedPositions);
			}

			SwingUtilities.invokeLater(frame::repaint);
			Thread.sleep(TICK_TIME);
		}
	}

	private static JFrame createAppFrame() {
		JFrame frame = new JFrame("EvoSnake");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.BLUE);

		frame.setLayout(new GridLayout(0, 2));
		return frame;
	}
}
