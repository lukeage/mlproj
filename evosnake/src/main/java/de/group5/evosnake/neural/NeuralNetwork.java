package de.group5.evosnake.neural;

import java.util.ArrayList;

public class NeuralNetwork{
	private ArrayList<Layer> layers;

	public ArrayList<Layer> getLayers() {
		return layers;
	}

	public void setLayers(ArrayList<Layer> layers) {
		this.layers = layers;
	}

	public int getNumFeatures() {
		return numFeatures;
	}

	public void setNumFeatures(int numFeatures) {
		this.numFeatures = numFeatures;
	}

	private int numFeatures;
	public NeuralNetwork(int numFeatures){
		this.layers = new ArrayList<Layer>();
		this.numFeatures = numFeatures;
	}

	public void addLayer(int numNeurons, ActivationFunction activation) {
		layers.add( new Layer(numNeurons, activation));
	}
	public int getNumWeights(){
		int numWeights = 0;
		int inpLen= numFeatures;
		for(int i=0; i<layers.size();i++){
			numWeights += inpLen*layers.get(i).getNumNeurons()+1;
			inpLen = layers.get(i).getNumNeurons();
		}
		return numWeights;
	}
	public void setWeights(double[] weights){
		int runInd=0; // starting index to take new weights
		int inpLen=numFeatures; // input length of the prev layer
		for(int i=0; i<layers.size();i++){
			Layer layer = layers.get(i); // create new instance of layer that is updated
			int weightsRequested = layer.getNumNeurons()*inpLen; // how many weights are needed in the layer
			double[] w = new double[weightsRequested]; // holds the weights temporarily
			for(int j = 0 ; j<weightsRequested;j++){
				w[j]=weights[runInd+j];		//take weights
			}
			layer.setWeights(w); // set weights in layer
			layer.setBias(weights[runInd+weightsRequested]); // use next weight as bias
			inpLen = layer.getNumNeurons();	//update input length for next layer
			runInd += weightsRequested+1; // move index by num weights take +1 for bias
			layers.set(i,layer); // replace layer with update version
		}
	}
	public double[] predict(double[]input){
		for(int i =0 ; i< layers.size(); i++){
			input = layers.get(i).propagate(input);
		}
		return input;
	}
	/*
//testing
	public static void main(String[] args){
		Random r = new Random();
		double[] inp = new double[]{1f,-2f,3f,-4f,5f};
		NeuralNetwork NN = new NeuralNetwork(inp.length);
		NN.addLayer(7,Layer.RELU);
		NN.addLayer(4,Layer.RELU);
		NN.addLayer(3,Layer.SOFTMAX);
		System.out.println(NN.getNumWeights());
		double[] weights = new double[NN.getNumWeights()];
		for (int i = 0;i< NN.getNumWeights();i++)
			weights[i] = r.nextDouble();
		NN.setWeights(weights);
		double[] out = NN.predict(inp);
		for(int i=0; i< out.length;i++)
			System.out.println(out[i]);
	}*/
}


/*
import java.util.Random;

public class NeuralNetwork {

	private final int numberOfWeights;

	private double[] weights;

	private double[] layerIN;
	private double[] layer1;
	private double[] layer2;
	private double[] layerOUT;

	private double[] bias1;
	private double[] bias2;

	public NeuralNetwork(int inputSize, int layer1Size, int layer2Size, int outputSize) {

		this.numberOfWeights = inputSize*layer1Size+layer1Size*layer2Size+layer2Size*outputSize;

		//zufällige gewichte zwischen 0 und 1
		Random rand = new Random();
		double[] rweights = new double[this.numberOfWeights];
		for(int i =0; i<this.numberOfWeights;i++){
			rweights[i] = rand.nextDouble();
		}
		this.weights = rweights;

		this.layerIN = new double[inputSize];
		this.layer1 = new double[layer1Size];
		this.layer2 = new double[layer2Size];
		this.layerOUT = new double[outputSize];

		this.bias1 = new double[layer1Size];
		this.bias2 = new double[layer2Size];

		//fill bias1 and bias2
		for(int i =0; i < bias1.length; i++){
			bias1[i] = (double)0.1;
		}
		for(int i = 0; i< bias2.length; i++){
			bias2[i] = (double)0.1;
		}
	}

	public double[] getWeights() {
		return weights;
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	public int getNumberOfWeights(){
		return numberOfWeights;
	}

	public double[] predict(double[] input) {

		// Layer IN
		System.out.println("set Input");
		this.layerIN = input;

		// Layer 1
		System.out.println("compute layer1");
		for(int i = 0; i < layer1.length; i++){
			double sum = 0;
			double bias = bias1[i];
			for(int j = 0; j < layerIN.length; j++){
				double w_ij = weights[(i*layerIN.length)+ j];
				double a_j = layerIN[j];
				sum += w_ij*a_j;
			}
			layer1[i] = reLu(sum+bias);
		}

		// Layer 2
		System.out.println("compute layer2");
		for(int i = 0; i < layer2.length; i++){
			double sum = 0;
			double bias = bias2[i];
			for(int j = 0; j < layer1.length; j++){
				double w_ij = weights[(i*layer1.length)+ j];
				double a_j = layer1[j];
				sum += w_ij*a_j;
			}
			layer2[i] = reLu(sum+bias);
		}

		// Layer OUT

		for(int i = 0; i < layerOUT.length; i++){
			double sum = 0;
			//bias = bias2[i];
			for(int j = 0; j < layer2.length; j++){
				double w_ij = weights[(i*layer2.length)+j];
				double a_j = layer2[j];
				sum += w_ij*a_j;
				layerOUT[i] = reLu(sum); //+bias;
			}
		}

		System.out.println("compute softmax");
		double sumLayerOut = 0;
		for(int i = 0; i < layerOUT.length; i++){
			sumLayerOut += Math.exp(layerOUT[i]);
		}
		for(int i = 0; i < layerOUT.length; i++){
			layerOUT[i] = Math.exp(layerOUT[i])/sumLayerOut;
		}

		return layerOUT;
	}

	private double sigmoid(double x){
		return 1/(1+Math.exp(-x));		//TODO: Implement
	}

	private double reLu(double x){
		if(x<0){
			return 0;
		}else {
			return x;
		}
	}



}
*/