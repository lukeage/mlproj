package de.group5.evosnake.neural;

public enum ActivationFunction {
	RELU,
	SIGMOID,
	SOFTMAX
}
