package de.group5.evosnake.neural;


public class Layer {

    private int numNeurons;
    private ActivationFunction activation;
    private double[] weights;
    private double bias;

    Layer(int numNeurons, ActivationFunction activation) {
        this.activation = activation;
        this.numNeurons = numNeurons;
    }
    void setWeights(double[] weights) {
        this.weights = weights;
    }
    void setBias(double bias) {
        this.bias = bias;
    }
    private double[]reLu(double[] input){
        for(int i = 0; i<input.length; i++){
            if (input[i]<0)
                input[i]=0;
        }
        return input;
    }
    private double[]sigmoid(double[] input){
        for(int i = 0; i<input.length; i++){
           input[i] = 1/(1+Math.exp(-input[i]));
        }
        return input;
    }
    private double[]softmax(double[] input){
        double acc=0;
        for (double v : input) {
            acc += Math.exp(v);
        }
        for(int i = 0; i<input.length; i++){
            input[i] = Math.exp(input[i])/acc;
        }

        return input;
    }

    public int getNumNeurons() {
        return numNeurons;
    }

    public ActivationFunction getActivation() {
        return activation;
    }

    double[] propagate(double[] input){
        double[] output = new double[numNeurons];
        for(int i = 0; i< numNeurons; i++){
            double acc=0;
            for(int j=0; j<input.length; j++){
                acc += weights[j+(i*input.length)]*input[j];
            }
            acc += bias;
            output[i] = acc;
        }
        switch (activation){
            case RELU: return reLu(output);
            case SIGMOID: return sigmoid(output);
            case SOFTMAX: return softmax(output);
            default: return output;
        }
    }
    /*
 //testing
    public static void main(String[] args){
        Random r = new Random();
        Layer layertest = new Layer(3,SOFTMAX);
        double[] testinp = {1f,1.5f,2f,3.5f,4f};
        int neededweights = testinp.length* layertest.numNeurons;
        double[] testweights = new double[neededweights];
        for (int i = 0 ; i< neededweights; i++) {
            testweights[i] = r.nextDouble();
        }
        layertest.setWeights(testweights);
        layertest.setBias(0.1f);
        double[] output = layertest.propagate(testinp);
        for (int i = 0 ; i< output.length ; i++)
            System.out.println(output[i]);

    }*/
}

