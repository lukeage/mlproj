package de.group5.evosnake.agent;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;

import de.group5.evosnake.game.*;

public class HumanPlayer extends Agent implements KeyListener, KeyEventDispatcher {

	private Direction currentDirection = Direction.STRAIGHT;

	public HumanPlayer() {
		super(null);
	}

	@Override
	public Direction nextMove(GameState gameState) {
		Direction result = currentDirection;
		//transformBoard(gameState.getBoard());
		currentDirection = Direction.STRAIGHT;
		return result;
	}

	@Override
	public String toString() {
		return "HumanPlayer{}";
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		if (e.getID() != KeyEvent.KEY_PRESSED) {
			return false;
		}

		int keyCode = e.getKeyCode();
		switch (keyCode) {
		case KeyEvent.VK_LEFT:
			currentDirection = Direction.LEFT;
			break;
		case KeyEvent.VK_RIGHT:
			currentDirection = Direction.RIGHT;
			break;

		default:
			break;
		}
		return false;
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}


	//test
	/*

	private double[] transformBoard(Field[][] board) {
		// TODO Implement this

		//assume rectangular board
		int boardWidth = board.length;
		int boardHeight = board[0].length;

		Position boardDimensions = new Position(boardWidth, boardHeight);
		//find interesting fields. Expecting 1 head, multiple tails and 1 food


		HashMap<Field, ArrayList<Position>> interestingFields = findInterestingFields(board);
		ArrayList<Position> head = interestingFields.get(Field.HEAD);
		ArrayList<Position> tail = interestingFields.get(Field.TAIL);
		ArrayList<Position> food = interestingFields.get(Field.FOOD);

		//calculate features
		int distToDeath_North = calcDistToDeath(boardDimensions, interestingFields, AbsoluteDirection.NORTH);
		int distToDeath_West = calcDistToDeath(boardDimensions, interestingFields, AbsoluteDirection.WEST);
		int distToDeath_South = calcDistToDeath(boardDimensions, interestingFields, AbsoluteDirection.SOUTH);
		int distToDeath_East = calcDistToDeath(boardDimensions, interestingFields, AbsoluteDirection.EAST);

		//Testing
		System.out.println("North: " + distToDeath_North);
		System.out.println("West: " + distToDeath_West);
		System.out.println("South: " + distToDeath_South);
		System.out.println("East: " + distToDeath_East);
		System.out.println("----------------");




		return new double[0];
	}

	private HashMap<Field, ArrayList<Position>> findInterestingFields(Field[][] board) {

		//scan board column by column and find non-empty fields.
		// Assuming rectangular board.
		HashMap<Field, ArrayList<Position>> interestingFieldsMap = new HashMap<>();
		ArrayList<Position> head = new ArrayList<>();
		ArrayList<Position> tail = new ArrayList<>();
		ArrayList<Position> food = new ArrayList<>();

		for(int i=0;i<board.length;i++){
			for (int j=0;j<board[i].length;j++){
				if(board[i][j]!=Field.EMPTY){
					switch(board[i][j]){
						case HEAD: head.add(new Position(i,j));
						case TAIL: tail.add(new Position(i,j));
						case FOOD: food.add(new Position(i,j));
					}
				}
			}
		}
		interestingFieldsMap.put(Field.HEAD, head);
		interestingFieldsMap.put(Field.TAIL, tail);
		interestingFieldsMap.put(Field.FOOD, food);

		return interestingFieldsMap;
	}

	private int calcDistToDeath(Position boardDimensions, HashMap<Field,ArrayList<Position>> interestingFields, AbsoluteDirection dir){

		Position head = interestingFields.get(Field.HEAD).get(0);
		ArrayList<Position> tail = interestingFields.get(Field.TAIL);

		int limit = (dir.getX()+1)*boardDimensions.getX()/2;
		int temp, tick;

		switch(dir){
			case EAST: {
				limit = boardDimensions.getX();
				temp = head.getX();
				tick = dir.getX();
				break;
			}
			case WEST: {
				limit = 0;
				temp = head.getX();
				tick = dir.getX();
				break;
			}
			case NORTH: {
				limit = 0;
				temp = head.getY();
				tick = dir.getY();
				break;
			}
			case SOUTH: {
				limit = boardDimensions.getY();
				temp = head.getY();
				tick = dir.getY();
				break;
			}

			default: return 0; //TODO: implement exception
		}
		int dist = 0;
		int x,y;

		//move in direction and check for evil fields
		while(temp!=limit){
			if(dir.getX()==0){
				x=head.getX();y=temp;}
			else{x=temp;y=head.getY();}
			dist ++;
			temp += tick;
			Position p = new Position(x,y);
			for(Position t:tail){
				if (p.equals(t)){
					//going to crash at Position (x, head.getY())
					break;
				}
			}
		}
		return dist;
	}*/
}
