
package de.group5.evosnake.agent;

import de.group5.evosnake.game.*;
import de.group5.evosnake.genetics.GeneticCode;
import de.group5.evosnake.neural.ActivationFunction;
import de.group5.evosnake.neural.Layer;
import de.group5.evosnake.neural.NeuralNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.lang.Object;

public class AlgorithmicAgent extends Agent {


    public AlgorithmicAgent() {
        super(null);
    }

    public Direction nextMove(GameState gameState) {
        double[] transformedBoard = transformBoard(gameState);
        return getDirectionFromOutput(transformedBoard);
    }

    private double[] getWeights(GeneticCode geneticCode) {
        return geneticCode.getGenes();
    }

    private Direction getDirectionFromOutput(double[] output) {
        // TODO Implement this

        //testing

        //check if snake is in danger.
        AbsoluteDirection[] dir = new AbsoluteDirection[]{AbsoluteDirection.NORTH,AbsoluteDirection.WEST,AbsoluteDirection.SOUTH,AbsoluteDirection.EAST};

        int distToDeath_Straight = (int)output[0];
        int distToDeath_Left = (int)output[1];
        int distToDeath_Right = (int)output[2];
        int distToFoodStraight =(int)output[3];
        int distToFoodRight =(int)output[4];

        HashMap<Direction, Integer> distanceMap = new HashMap<>();
        distanceMap.put(Direction.STRAIGHT,distToDeath_Straight);
        distanceMap.put(Direction.LEFT,distToDeath_Left);
        distanceMap.put(Direction.RIGHT,distToDeath_Right);


        Direction try_dir = null;
        Direction try_dir_sec = null;
        int distToFood = 0;

        System.out.println("distToDeath_Straight: " + distToDeath_Straight);
        System.out.println("disttoDeath_Right: " + distToDeath_Right);
        System.out.println("distToDeath_Left: " + distToDeath_Left);

        System.out.println("distToFoodStraight: " + distToFoodStraight);
        System.out.println("distToFoodRight: " + distToFoodRight);



        if(distToFoodRight!=0){
            if(distToFoodRight>0){
                //try to approach food in Right direction
                try_dir = Direction.RIGHT;
                try_dir_sec = Direction.RIGHT;
                distToFood=distToFoodRight;
            }
            else {
                try_dir = Direction.LEFT;
                try_dir_sec = Direction.LEFT;
                distToFood= (-1)*distToFoodRight;
            }
        }

        if(distToFoodStraight!=0){
            if(distToFoodStraight>0){
                //try to approach food in Straight direction
                try_dir = Direction.STRAIGHT;
                try_dir_sec = Direction.STRAIGHT;
                distToFood=distToFoodStraight;
            }
            else {
                if(try_dir==null){
                    try_dir = Direction.RIGHT;
                    try_dir_sec = Direction.LEFT;
                    distToFood=(-1)*distToFoodStraight;
                }
            }
        }

        System.out.println("Yummi Food in : " + try_dir);
        System.out.println("or maybe in : " + try_dir_sec);
        Direction result = Direction.STRAIGHT;


        if(distanceMap.get(try_dir)>distToFood){
            result=try_dir;
        }
        else {
            if(distanceMap.get(try_dir_sec)>distToFood){
                result=try_dir_sec;
            }
            else {
                //way to food blocked. trying something different
                int distLeft = distanceMap.get(Direction.LEFT);
                int distRight = distanceMap.get(Direction.RIGHT);
                int distStraight = distanceMap.get(Direction.STRAIGHT);
                if(distLeft>distRight) {
                    if (distLeft > distStraight) {
                        result = Direction.LEFT;
                    } else {
                        result = Direction.STRAIGHT;
                    }
                }
                else {
                    if(distRight>distStraight){
                        result = Direction.RIGHT;
                    }
                    else {
                        result = Direction.STRAIGHT;
                    }
                }
            }
        }
        System.out.println("turning: " + result);
        return result;
    }


    private double[] transformBoard(GameState gameState) {
        // TODO Implement this

        Field[][] board = gameState.getBoard();
        AbsoluteDirection currentDirection = gameState.getPreviousDirection();

        //assume rectangular board
        int boardWidth = board.length;
        int boardHeight = board[0].length;
        Position boardDimensions = new Position(boardWidth, boardHeight);

        //find interesting fields. Expecting 1 head, multiple tails and 1 food
        HashMap<Field, ArrayList<Position>> interestingFields = findInterestingFields(board);

        int distToDeath_Straight = calcDistToDeath(boardDimensions, interestingFields, Direction.STRAIGHT, currentDirection);
        int distToDeath_Left = calcDistToDeath(boardDimensions, interestingFields, Direction.LEFT, currentDirection);
        int distToDeath_Right = calcDistToDeath(boardDimensions, interestingFields, Direction.RIGHT, currentDirection);
        Position distToFoodRelative = calcDistToFood(interestingFields, currentDirection);

        //representation: [distToDeath_Straight, distToDeath_Left, distToDeath_Right, distToFood_Straight, distToFood_Right]

        return new double[]{distToDeath_Straight,distToDeath_Left, distToDeath_Right, distToFoodRelative.getX(),distToFoodRelative.getY()};
    }

    private HashMap<Field, ArrayList<Position>> findInterestingFields(Field[][] board) {

        //scan board column by column and find non-empty fields.
        // Assuming rectangular board.
        HashMap<Field, ArrayList<Position>> interestingFieldsMap = new HashMap<>();
        ArrayList<Position> head = new ArrayList<>();
        ArrayList<Position> tail = new ArrayList<>();
        ArrayList<Position> food = new ArrayList<>();

        for(int i=0;i<board.length;i++){
            for (int j=0;j<board[i].length;j++){
                if(board[i][j]!=Field.EMPTY){
                    switch(board[i][j]){
                        case HEAD: {
                            head.add(new Position(i, j));
                            break;
                        }
                        case TAIL: {tail.add(new Position(i,j));
                            break;
                        }
                        case FOOD: {food.add(new Position(i,j));
                            break;
                        }
                    }
                }
            }
        }
        interestingFieldsMap.put(Field.HEAD, head);
        interestingFieldsMap.put(Field.TAIL, tail);
        interestingFieldsMap.put(Field.FOOD, food);

        return interestingFieldsMap;
    }

    private int calcDistToDeath(Position boardDimensions, HashMap<Field,ArrayList<Position>> interestingFields, Direction reldir, AbsoluteDirection currentDirection){

        Position head = interestingFields.get(Field.HEAD).get(0);
        ArrayList<Position> tail = interestingFields.get(Field.TAIL);

        AbsoluteDirection dir = currentDirection.turn(reldir);

        int temp, tick, limit;

        switch(dir){
            case EAST: {
                limit = boardDimensions.getX();
                temp = head.getX();
                tick = dir.getX();
                break;
            }
            case WEST: {
                limit = -1;
                temp = head.getX();
                tick = dir.getX();
                break;
            }
            case NORTH: {
                limit = -1;
                temp = head.getY();
                tick = dir.getY();
                break;
            }
            case SOUTH: {
                limit = boardDimensions.getY();
                temp = head.getY();
                tick = dir.getY();
                break;
            }

            default: return 0; //TODO: implement exception
        }
        int dist = 0;
        int x,y;

        //move in direction and check for evil fields
        while(temp!=limit){
            temp += tick;
            if(dir.getX()==0){
                x=head.getX();y=temp;}
            else{x=temp;y=head.getY();}
            dist ++;
            Position p = new Position(x,y);
            for(Position t:tail){
                if (p.equals(t)){
                    //going to bite itself at Position (x, head.getY())
                    temp=limit;
                }
            }
        }
        return dist;
    }

    private Position calcDistToFood(HashMap<Field,ArrayList<Position>> interestingFields, AbsoluteDirection currentDirection){

        Position head = interestingFields.get(Field.HEAD).get(0);
        Position food = interestingFields.get(Field.FOOD).get(0);

        Position pathToFood = new Position(food.getX()-head.getX(),food.getY()-head.getY());

        int distStraight, distRight;

        if(currentDirection.getX()==0){
            //moving along Y-Axis
            distStraight = (food.getY() - head.getY())*currentDirection.getY();
            distRight = (food.getX() - head.getX())*currentDirection.getY()*(-1);
        }
        else {
            //moving along X-Axis
            distRight = (food.getY() - head.getY())*currentDirection.getX();
            distStraight = (food.getX() - head.getX())*currentDirection.getX();
        }
        return new Position(distStraight, distRight);

    }

	private Direction AbsoluteToRelativeDirection(AbsoluteDirection abs, AbsoluteDirection current) {
		Direction dir;

		int newOffset = (current.getOffset() + Direction.LEFT.getOperation() + 4) % 4;

		if (abs.getOffset()==current.getOffset()) {
			dir = Direction.STRAIGHT;
		}
		else {
			if (newOffset==abs.getOffset()) {
				dir = Direction.LEFT;
			} else {
				dir = Direction.RIGHT;
			}
		}
		return dir;
	}

}
