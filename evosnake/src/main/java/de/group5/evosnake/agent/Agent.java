package de.group5.evosnake.agent;

import java.util.Objects;

import de.group5.evosnake.game.Direction;
import de.group5.evosnake.game.GameState;
import de.group5.evosnake.genetics.GeneticCode;

/**
 * Receives the GameState, returns a direction.
 */
public abstract class Agent {

	protected final GeneticCode geneticCode;

	protected Agent(GeneticCode geneticCode) {
		this.geneticCode = geneticCode;
	}

	public GeneticCode getGeneticCode() {
		return this.geneticCode;
	}

	public abstract Direction nextMove(GameState gameState);

	@Override
	public String toString() {
		return geneticCode.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Agent)) {
			return false;
		}
		Agent agent = (Agent) o;
		return geneticCode.equals(agent.geneticCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(geneticCode);
	}
}
