package de.group5.evosnake.agent;

import java.util.ArrayList;
import java.util.HashMap;

import de.group5.evosnake.game.*;
import de.group5.evosnake.genetics.GeneticCode;
import de.group5.evosnake.neural.ActivationFunction;
import de.group5.evosnake.genetics.GeneticAlgorithmImpl;
import de.group5.evosnake.neural.NeuralNetwork;

public class NeuralNetworkAgent extends Agent {

	private final NeuralNetwork neuralNetwork;

	public NeuralNetworkAgent(GeneticCode geneticCode, NeuralNetwork neuralNetwork) {
		super(geneticCode);
		this.neuralNetwork = neuralNetwork;
		neuralNetwork.setWeights(getWeights(getGeneticCode()));

	}

	public Direction nextMove(GameState gameState) {
		double[] transformedBoard = transformBoard(gameState);
		double[] output = neuralNetwork.predict(transformedBoard);
		return getDirectionFromOutput(output);
	}

	private double[] getWeights(GeneticCode geneticCode) {
		return geneticCode.getGenes();
	}

	private Direction getDirectionFromOutput(double[] output) {
		// TODO Implement this

		//find max
		double temp = 0;
		int maxIndex = 0;
		for (int i = 0; i < output.length; i++) {
			if (output[i] > temp) {
				temp = output[i];
				maxIndex = i;
			}
		}

		Direction[] directions = new Direction[]{Direction.STRAIGHT, Direction.LEFT, Direction.RIGHT};

		return directions[maxIndex];
	}

	private double[] transformBoard(GameState gameState) {
		// TODO Implement this

		Field[][] board = gameState.getBoard();
		AbsoluteDirection currentDirection = gameState.getPreviousDirection();

		//assume rectangular board
		int boardWidth = board.length;
		int boardHeight = board[0].length;
		Position boardDimensions = new Position(boardWidth, boardHeight);

		//find interesting fields. Expecting 1 head, multiple tails and 1 food
		HashMap<Field, ArrayList<Position>> interestingFields = findInterestingFields(board);

		int distToDeath_Straight = calcDistToDeath(boardDimensions, interestingFields, Direction.STRAIGHT, currentDirection);
		int distToDeath_Left = calcDistToDeath(boardDimensions, interestingFields, Direction.LEFT, currentDirection);
		int distToDeath_Right = calcDistToDeath(boardDimensions, interestingFields, Direction.RIGHT, currentDirection);
		Position distToFoodRelative = calcDistToFood(interestingFields, currentDirection);

		//representation: [distToDeath_Straight, distToDeath_Left, distToDeath_Right, distToFood_Straight, distToFood_Right]

		return new double[]{distToDeath_Straight, distToDeath_Left, distToDeath_Right, distToFoodRelative.getX(), distToFoodRelative.getY()};
	}

	private HashMap<Field, ArrayList<Position>> findInterestingFields(Field[][] board) {

		//scan board column by column and find non-empty fields.
		// Assuming rectangular board.
		HashMap<Field, ArrayList<Position>> interestingFieldsMap = new HashMap<>();
		ArrayList<Position> head = new ArrayList<>();
		ArrayList<Position> tail = new ArrayList<>();
		ArrayList<Position> food = new ArrayList<>();

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j] != Field.EMPTY) {
					switch (board[i][j]) {
						case HEAD: {
							head.add(new Position(i, j));
							break;
						}
						case TAIL: {
							tail.add(new Position(i, j));
							break;
						}
						case FOOD: {
							food.add(new Position(i, j));
							break;
						}
					}
				}
			}
		}
		interestingFieldsMap.put(Field.HEAD, head);
		interestingFieldsMap.put(Field.TAIL, tail);
		interestingFieldsMap.put(Field.FOOD, food);

		return interestingFieldsMap;
	}

	private int calcDistToDeath(Position boardDimensions, HashMap<Field, ArrayList<Position>> interestingFields, Direction reldir, AbsoluteDirection currentDirection) {

		Position head = interestingFields.get(Field.HEAD).get(0);
		ArrayList<Position> tail = interestingFields.get(Field.TAIL);

		AbsoluteDirection dir = currentDirection.turn(reldir);

		int temp, tick, limit;

		switch (dir) {
			case EAST: {
				limit = boardDimensions.getX();
				temp = head.getX();
				tick = dir.getX();
				break;
			}
			case WEST: {
				limit = -1;
				temp = head.getX();
				tick = dir.getX();
				break;
			}
			case NORTH: {
				limit = -1;
				temp = head.getY();
				tick = dir.getY();
				break;
			}
			case SOUTH: {
				limit = boardDimensions.getY();
				temp = head.getY();
				tick = dir.getY();
				break;
			}

			default:
				return 0; //TODO: implement exception
		}
		int dist = 0;
		int x, y;

		//move in direction and check for evil fields
		while (temp != limit) {
			temp += tick;
			if (dir.getX() == 0) {
				x = head.getX();
				y = temp;
			} else {
				x = temp;
				y = head.getY();
			}
			dist++;
			Position p = new Position(x, y);
			for (Position t : tail) {
				if (p.equals(t)) {
					//going to bite itself at Position (x, head.getY())
					temp = limit;
				}
			}
		}
		return dist;
	}

	private Position calcDistToFood(HashMap<Field, ArrayList<Position>> interestingFields, AbsoluteDirection currentDirection) {

		Position head = interestingFields.get(Field.HEAD).get(0);
		Position food = interestingFields.get(Field.FOOD).get(0);

		Position pathToFood = new Position(food.getX() - head.getX(), food.getY() - head.getY());

		int distStraight, distRight;

		if (currentDirection.getX() == 0) {
			//moving along Y-Axis
			distStraight = (food.getY() - head.getY()) * currentDirection.getY();
			distRight = (food.getX() - head.getX()) * currentDirection.getY() * (-1);
		} else {
			//moving along X-Axis
			distRight = (food.getY() - head.getY()) * currentDirection.getX();
			distStraight = (food.getX() - head.getX()) * currentDirection.getX();
		}
		return new Position(distStraight, distRight);

	}
}