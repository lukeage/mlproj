package de.group5.evosnake.genetics;

import java.util.Map;

import de.group5.evosnake.agent.Agent;

public class Generation {

	private final Map<Agent, Integer> scoredAgents;

	private Map.Entry<Agent, Integer> bestEntry;

	public Generation(Map<Agent, Integer> scoredAgents) {
		this.scoredAgents = scoredAgents;
	}

	public Map<Agent, Integer> getScoredAgents() {
		return scoredAgents;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (Agent agent : scoredAgents.keySet()) {
			if (scoredAgents.get(agent) != null) {
				s.append("\n").append(agent.toString()).append("score=").append(scoredAgents.get(agent).toString());
			} else {
				s.append("\n").append(agent.toString());
			}
		}
		return s.toString();
	}

	public String compactToString() {
		StringBuilder s = new StringBuilder();
		for (Agent agent : scoredAgents.keySet()) {
			s.append("\n Score=").append(scoredAgents.get(agent).toString());
		}
		return s.toString();
	}

	public Agent getBestAgent() {
		if (bestEntry == null) {
			calculateBestEntry();
		}
		return bestEntry.getKey();
	}

	private void calculateBestEntry() {
		bestEntry = scoredAgents.entrySet().stream().max(Map.Entry.comparingByValue()).get();
	}

	public void printStatistics() {
		if (bestEntry == null) {
			calculateBestEntry();
		}
		int maxScore = bestEntry.getValue();
		double average = scoredAgents.entrySet().stream().mapToInt(Map.Entry::getValue).average().getAsDouble();

		System.out.println("Generation has maximum score of " + maxScore + " and average score of " + average);
	}

	public int getMaxScore() {
		if (bestEntry == null) {
			calculateBestEntry();
		}
		return bestEntry.getValue();
	}
}
