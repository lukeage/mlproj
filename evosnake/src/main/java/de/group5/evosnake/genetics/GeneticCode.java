package de.group5.evosnake.genetics;

import java.util.Arrays;

/**
 * Encodes the behaviour of the agent. Can be mutated by the GeneticAlgorithm.
 */
public class GeneticCode {



	// Also weights for the neural network.
	private final double[] genes;

	public GeneticCode(double[] genes) {
		this.genes = genes;
	}

	public double[] getGenes() {
		return genes;
	}

	public double getGeneAt(int index) {return genes[index];}

	@Override
	public String toString() {
		return "genes={" + Arrays.toString(genes) +
				'}';
	}
}
