package de.group5.evosnake.genetics;

import java.io.*;
import java.util.*;
import de.group5.evosnake.agent.Agent;
import de.group5.evosnake.agent.NeuralNetworkAgent;
import de.group5.evosnake.neural.ActivationFunction;
import de.group5.evosnake.neural.*;


public class GeneticAlgorithmImpl implements GeneticAlgorithm {
	public static int GENE_LENGTH;
	public static int GENERATION_LENGTH = 200;
	public static float MUTATION_RATE = 0.1f;
	public static float MUTATION_RANGE = 0.1f;
	public static int[] LEARNING_RATE_THRESHOLDS = {2500,6000,6500,7800};
	private Random r;
	private NeuralNetwork modelNetwork;
	public GeneticAlgorithmImpl(){
		r = new Random();
		modelNetwork = getNewNetwork();
		GENE_LENGTH = modelNetwork.getNumWeights();
	}

	/**
	 * returns initial weights based on a zero mean gaussian distribution with sigma = 1.0
	 * @return initialGeneration
	 */
	public Generation initializeGeneration() {
		Map<Agent, Integer> init_gen = new HashMap<Agent,Integer>();
		for(int i= 0; i<GENERATION_LENGTH;i++){
			double [] genes = new double[GENE_LENGTH];
			for(int j=0; j<GENE_LENGTH;j++){
				genes[j]= r.nextGaussian();
			}
			init_gen.put(new NeuralNetworkAgent(new GeneticCode(genes), getNewNetwork()), null);
			//init_gen.put(new NeuralNetworkAgent(new GeneticCode(genes)), r.nextInt(50));//random performance data for testing
		}
		return new Generation(init_gen);
	}

	/**
	 * Creates new weight genomes based on the previous generation and its performance.
	 * @param previousGeneration Agents and matching performance ratings from the previous generation
	 * @return nextGeneration
	 */
	public Generation getNextGeneration(Generation previousGeneration) {
		//System.out.println(previousGeneration.toString());
		// System.out.println(previousGeneration.compactToString());
		double mutation_range_modifier =2.0f;
		double avg_score = previousGeneration.getScoredAgents().entrySet().stream().mapToInt(Map.Entry::getValue).average().getAsDouble();
		if (avg_score>LEARNING_RATE_THRESHOLDS[0]){
			mutation_range_modifier =1.0f;
			if(avg_score>LEARNING_RATE_THRESHOLDS[1]){
				mutation_range_modifier=0.5f;
				if(avg_score>LEARNING_RATE_THRESHOLDS[2]){
					mutation_range_modifier=0.1f;
					if(avg_score>LEARNING_RATE_THRESHOLDS[3]){
						mutation_range_modifier=0.05f;
					}
				}
			}

		}

		ArrayList<GeneticCode> parentsA = selection(previousGeneration.getScoredAgents());
		ArrayList<GeneticCode> parentsB = selection(previousGeneration.getScoredAgents());
		/*
		System.out.println("\n ParentsA:"); //Testing
		for(GeneticCode g : parentsA){
			System.out.println(g.toString());
		}
		System.out.println("\n ParentsB:");
		for(GeneticCode g : parentsB){
			System.out.println(g.toString());
		}*/
		GeneticCode[] nextGenGenomes = recombination(parentsA,parentsB);
		/*
		System.out.println("\n Recombined:");
		for(int i= 0 ; i< GENERATION_LENGTH;i++){
			System.out.println(nextGenGenomes[i].toString());
		}*/
		nextGenGenomes = mutation(nextGenGenomes,mutation_range_modifier);
		/*
		System.out.println("\n Mutated:");
		for(int i= 0 ; i< GENERATION_LENGTH;i++){
			System.out.println(nextGenGenomes[i].toString());
		}*/
		HashMap<Agent,Integer> nextGenMap = new HashMap<Agent, Integer>();
		for(int i= 0 ; i< GENERATION_LENGTH;i++){
			nextGenMap.put(new NeuralNetworkAgent(nextGenGenomes[i], getNewNetwork()), null);
		}

		return new Generation(nextGenMap);
	}

	public NeuralNetwork getNewNetwork() {
		NeuralNetwork network;
		if(modelNetwork ==  null){ //set network architecture
		network = new NeuralNetwork(5);
		network.addLayer(8, ActivationFunction.RELU);
		network.addLayer(3, ActivationFunction.SOFTMAX);}
		else{
			network = new NeuralNetwork(modelNetwork.getNumFeatures());
			for(int i=0 ; i< modelNetwork.getLayers().size(); i++){
				network.addLayer(modelNetwork.getLayers().get(i).getNumNeurons(),modelNetwork.getLayers().get(i).getActivation());
			}
		}
		return network;
	}

	/*
	public NeuralNetwork getNewNetwork() {
		NeuralNetwork network = new NeuralNetwork(5);
		network.addLayer(7, ActivationFunction.RELU);
		network.addLayer(3, ActivationFunction.SOFTMAX);
		return network;
	}
	*/
	/**
	 * Selects Genomes for later recombination. Better performing genomes have higher chance to be accepted.
	 * @param scoredAgents benchmarked agents
	 * @return selectedGenomes
	 */
	private ArrayList<GeneticCode> selection(Map<Agent,Integer> scoredAgents){
		int maxValue = 0;
		for (int v : scoredAgents.values()){
			if (v> maxValue) maxValue=v;
		}
		ArrayList<GeneticCode> selected = new ArrayList<GeneticCode>();
		Agent[] agents = scoredAgents.keySet().toArray(new Agent[scoredAgents.size()]);
		while(selected.size()<GENERATION_LENGTH){
			int pick = r.nextInt(agents.length);
			if(r.nextInt(maxValue) <= scoredAgents.get(agents[pick])){
				selected.add(agents[pick].getGeneticCode());
			}
		}
		return selected;
	}
	/**
	 * recombines selected genomes to new child genomes
	 * @param parentA genomes of the first parent
	 * @param parentB genomes of the second parent
	 * @return recombinedGenomes
	 */
	private GeneticCode[] recombination (ArrayList<GeneticCode> parentA, ArrayList<GeneticCode> parentB){
		GeneticCode[] recombinedGenomes = new GeneticCode[GENERATION_LENGTH];
		for(int i=0; i<GENERATION_LENGTH; i++){
			double[] genes = new double[GENE_LENGTH];
			for(int j=0; j<GENE_LENGTH;j++){
				if(r.nextBoolean()){
					genes[j] = parentA.get(i).getGeneAt(j);}
				else{
					genes[j] = parentB.get(i).getGeneAt(j);}
			}
			recombinedGenomes[i] = new GeneticCode(genes);
		}
		return recombinedGenomes;
	}

	/**
	 * mutates genomes based on the mutation rate and range
	 * @param genomes recombined genomes to be mutated
	 * @return mutatedGenomes
	 */
	private GeneticCode[] mutation (GeneticCode[] genomes, double mutation_range_modifier){
		GeneticCode[] mutatedGenomes = new GeneticCode[GENERATION_LENGTH];
		for(int i = 0; i<GENERATION_LENGTH;i++){
			double[] genes = new double[GENE_LENGTH];
			for(int j = 0; j<GENE_LENGTH; j++){
				if(r.nextDouble()<MUTATION_RATE){
					genes[j] = genomes[i].getGeneAt(j)+((r.nextDouble( )-0.5f)*2*MUTATION_RANGE*mutation_range_modifier);
				}else{
					genes[j] = genomes[i].getGeneAt(j);
				}
			}
			mutatedGenomes[i]= new GeneticCode(genes);
		}
		return mutatedGenomes;
	}

	public void saveModel(String filename, Generation generation) throws IOException {
		System.out.println("Saving model to file " + filename);

		String path ="data"+File.separator+"models";
		File output = new File(path, filename+".model");
		if(!output.createNewFile()){
			System.out.println("File creation failed!");
			System.out.println("Filename might already be occupied.");
			Boolean free = false;
			int i =0;
			while(!free){
				i++;
				output = new File(path, filename+i+".model");
				free = output.createNewFile();
			}
			System.out.println("Saved file as "+ filename+i+".model instead!");
		}
		FileOutputStream fos = new FileOutputStream(output);
		DataOutputStream dos = new DataOutputStream(fos);
		dos.writeInt(GENERATION_LENGTH);
		dos.flush();
		dos.writeInt(GENE_LENGTH);
		dos.flush();
		dos.writeFloat(MUTATION_RANGE);
		dos.flush();
		dos.writeFloat(MUTATION_RATE);
		dos.flush();
		dos.writeInt(modelNetwork.getNumFeatures());
		dos.flush();
		dos.writeInt(modelNetwork.getLayers().size());
		dos.flush();
		for (int i=0 ; i < modelNetwork.getLayers().size();i++){
			dos.writeInt(modelNetwork.getLayers().get(i).getNumNeurons());
			dos.flush();
			dos.writeInt(modelNetwork.getLayers().get(i).getActivation().ordinal());
			dos.flush();
		}
		Iterator<Map.Entry<Agent,Integer>> entryIterator = generation.getScoredAgents().entrySet().iterator();
		while(entryIterator.hasNext()){
			Map.Entry<Agent,Integer> e = entryIterator.next();
			double[] genes  = e.getKey().getGeneticCode().getGenes();
			for(int i=0; i<genes.length; i++){
				dos.writeDouble(genes[i]);
				dos.flush();
			}
			if(e.getValue()!=null)
				dos.writeInt(e.getValue());
			else
				dos.writeInt(0);
			dos.flush();
		}
		dos.close();
		fos.close();
	}

	public Generation loadModel(String filename) throws IOException{
		String path ="data"+File.separator+"models";
		File input = new File(path, filename+ ".model");
		if(!input.isFile())
			System.out.println(filename +" does not exist!");
		FileInputStream fis =  new FileInputStream(input);
		DataInputStream dis = new DataInputStream(fis);
		GENERATION_LENGTH= dis.readInt();
		GENE_LENGTH= dis.readInt();
		MUTATION_RANGE= dis.readFloat();
		MUTATION_RATE= dis.readFloat();
		int numFeatures= dis.readInt();
		NeuralNetwork modelNetwork = new NeuralNetwork(numFeatures);
		int numLayers = dis.readInt();
		for(int i = 0; i < numLayers ; i++){
			int numneurons = dis.readInt();
			int activation = dis.readInt();
			modelNetwork.addLayer(numneurons, ActivationFunction.values()[activation]);
		}
		this.modelNetwork = modelNetwork;
		Map<Agent,Integer> gen = new HashMap<>();
		for(int i = 0; i<GENERATION_LENGTH; i++){
			double[] genes = new double[GENE_LENGTH];
			int score;
			for(int j = 0; j<GENE_LENGTH;j++){
				genes[j] = dis.readDouble();
			}
			score = dis.readInt();
			gen.put(new NeuralNetworkAgent(new GeneticCode(genes),getNewNetwork()),score);
		}
		Generation out = new Generation(gen);
		dis.close();
		fis.close();

		return out;
	}



}
