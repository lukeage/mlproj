package de.group5.evosnake.genetics;

import java.io.IOException;

public interface GeneticAlgorithm {

	Generation initializeGeneration();

	Generation getNextGeneration(Generation previousGeneration);

}
