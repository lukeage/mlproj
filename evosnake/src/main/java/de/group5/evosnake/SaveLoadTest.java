package de.group5.evosnake;

import de.group5.evosnake.game.EvolutionController;
import de.group5.evosnake.genetics.Generation;
import de.group5.evosnake.genetics.GeneticAlgorithm;
import de.group5.evosnake.genetics.GeneticAlgorithmImpl;

import java.io.IOException;

public class SaveLoadTest {

    public static void main(String[] args) throws IOException {

        GeneticAlgorithmImpl geneticAlgorithm = new GeneticAlgorithmImpl();
        Generation currentGeneration = geneticAlgorithm.initializeGeneration();
        Generation nextGeneration;
        EvolutionController evolutionController = new EvolutionController();
        evolutionController.scoreGeneration(currentGeneration); // test save scored agents
        geneticAlgorithm.saveModel("test",currentGeneration);
        nextGeneration = geneticAlgorithm.getNextGeneration(currentGeneration);
        currentGeneration = nextGeneration;



        Generation gen  = geneticAlgorithm.loadModel("test");

        System.out.println(GeneticAlgorithmImpl.GENERATION_LENGTH);
        System.out.println(GeneticAlgorithmImpl.MUTATION_RANGE);
        System.out.println(GeneticAlgorithmImpl.MUTATION_RATE);
        System.out.println(GeneticAlgorithmImpl.GENE_LENGTH);
    }
}
