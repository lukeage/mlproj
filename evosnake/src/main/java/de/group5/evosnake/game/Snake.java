package de.group5.evosnake.game;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

import static de.group5.evosnake.game.SnakeController.BOARD_HEIGHT;
import static de.group5.evosnake.game.SnakeController.BOARD_WIDTH;

public class Snake {

	public static final int INITIAL_SNAKE_LENGTH = 4;

	private Deque<Position> parts;

	private AbsoluteDirection currentDirection;

	public Snake() {
		parts = new LinkedBlockingDeque<>();
		for (int i = 0; i < INITIAL_SNAKE_LENGTH; i++) {
			parts.addLast(new Position(BOARD_WIDTH / 2, BOARD_HEIGHT / 2 + i));
		}
		currentDirection = AbsoluteDirection.NORTH;
	}

	public Deque<Position> getParts() {
		return parts;
	}

	public AbsoluteDirection getCurrentDirection() {
		return currentDirection;
	}

	public void turn(Direction direction) {
		currentDirection = currentDirection.turn(direction);
	}
}
