package de.group5.evosnake.game;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import de.group5.evosnake.agent.Agent;

/**
 * Controls the game by calling nextMove() on the agent, processing the result state after that move, then calling
 * nextMove() again with the new state...
 */
public class SnakeController {

	public static final int BOARD_WIDTH = 30;
	public static final int BOARD_HEIGHT = 30;

	private final Random random;
	private final GameState gameState;
	private final Agent agent;
	private final Snake snake;
	private Position food;
	private boolean foundFoodThisTick = false;

	public SnakeController(Agent agent) {
		this(agent, System.currentTimeMillis());
	}

	public SnakeController(Agent agent, long seed) {
		this.random = new Random(seed);

		this.agent = agent;
		this.snake = new Snake();
		this.food = createRandomFood(snake);
		this.gameState = initializeGameState(snake, food);
	}

	public Set<Position> nextTick() throws GameOverException {
		foundFoodThisTick = false;
		Set<Position> changedPositions = new LinkedHashSet<>();

		Direction direction = agent.nextMove(gameState);
		snake.turn(direction);
		gameState.setPreviousDirection(snake.getCurrentDirection());
		Position head = snake.getParts().getFirst();
		Position newHead = new Position(head.getX() + snake.getCurrentDirection().getX(),
				head.getY() + snake.getCurrentDirection().getY());

		if (isOutOfBounds(newHead)) {
			// Went out out bounds. Snake is dead :(
			throw new GameOverException(getFinalScore());
		}
		if (isPositionInSnake(newHead, snake)) {
			// Bit itself. Snake is dead :(
			throw new GameOverException(getFinalScore());
		}

		// Snake lives. Move it on.
		gameState.getBoard()[snake.getParts().getFirst().getX()][snake.getParts().getFirst().getY()] = Field.TAIL;
		changedPositions.add(snake.getParts().getFirst());
		snake.getParts().addFirst(newHead);
		gameState.getBoard()[newHead.getX()][newHead.getY()] = Field.HEAD;
		changedPositions.add(newHead);

		if (newHead.equals(food)) {
			// Found food! The tail is not removed. Well done!
			foundFoodThisTick = true;
			food = createRandomFood(snake);
			gameState.getBoard()[food.getX()][food.getY()] = Field.FOOD;
			changedPositions.add(food);
		} else {
			// Remove the tail. Moving on.
			gameState.getBoard()[snake.getParts().getLast().getX()][snake.getParts()
					.getLast()
					.getY()] = Field.EMPTY;
			changedPositions.add(snake.getParts().getLast());
			snake.getParts().removeLast();
		}

		return changedPositions;
	}

	private Position createRandomFood(Snake snake) {
		Position newFood;
		do {
			int i = random.nextInt(BOARD_HEIGHT * BOARD_WIDTH);
			newFood = new Position(i / BOARD_WIDTH, i % BOARD_WIDTH);
		} while (isPositionInSnake(newFood, snake));
		return newFood;
	}

	private boolean isPositionInSnake(Position position, Snake snake) {
		for (Position snakePart : snake.getParts()) {
			if (position.equals(snakePart)) {
				return true;
			}
		}
		return false;
	}

	private boolean isOutOfBounds(Position newHead) {
		if (newHead.getX() < 0 || newHead.getX() >= BOARD_WIDTH) {
			return true;
		}
		if (newHead.getY() < 0 || newHead.getY() >= BOARD_HEIGHT) {
			return true;
		}
		return false;
	}

	protected GameState initializeGameState(Snake snake, Position food) {
		// Empty board
		Field[][] board = new Field[BOARD_WIDTH][BOARD_HEIGHT];
		for (int x = 0; x < BOARD_WIDTH; x++) {
			for (int y = 0; y < BOARD_HEIGHT; y++) {
				board[x][y] = Field.EMPTY;
			}
		}

		// Add the snake
		for (Position snakePart : snake.getParts()) {
			board[snakePart.getX()][snakePart.getY()] = Field.TAIL;
		}
		board[snake.getParts().getFirst().getX()][snake.getParts().getFirst().getY()] = Field.HEAD;

		// Add the food
		board[food.getX()][food.getY()] = Field.FOOD;

		return new GameState(board, AbsoluteDirection.NORTH);
	}

	public Field[][] getBoard() {
		return gameState.getBoard();
	}

	public Integer getFinalScore() {
		return 1 + (snake.getParts().size()-4)*100;
	}

	public boolean foundFoodThisTick() {
		return foundFoodThisTick;
	}
}
