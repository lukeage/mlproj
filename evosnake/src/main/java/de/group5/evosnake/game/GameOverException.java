package de.group5.evosnake.game;

public class GameOverException extends Throwable {
	private final int finalScore;

	public GameOverException(int finalScore) {
		this.finalScore = finalScore;
	}

	public int getFinalScore() {
		return finalScore;
	}
}
