package de.group5.evosnake.game;

public enum AbsoluteDirection {

	/**
	 * y-1
	 */
	NORTH(0, 0, -1),
	/**
	 * x-1
	 */
	WEST(1, -1, 0),
	/**
	 * y+1
	 */
	SOUTH(2, 0, 1),
	/**
	 * x+1
	 */
	EAST(3, 1, 0);

	private final int x;
	private final int y;
	private final int offset;

	AbsoluteDirection(int offset, int x, int y) {
		this.offset = offset;
		this.x = x;
		this.y = y;
	}

	public int getOffset() {
		return offset;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public AbsoluteDirection turn(Direction direction) {
		int newOffset = (this.offset + direction.getOperation() + 4) % 4;
		switch (newOffset) {
		case 0:
			return AbsoluteDirection.NORTH;
		case 1:
			return AbsoluteDirection.WEST;
		case 2:
			return AbsoluteDirection.SOUTH;
		case 3:
			return AbsoluteDirection.EAST;
		}
		return null;
	}
}
