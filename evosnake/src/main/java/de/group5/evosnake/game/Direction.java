package de.group5.evosnake.game;

public enum Direction {

	LEFT(1),
	RIGHT(-1),
	STRAIGHT(0);

	private final int operation;

	Direction(int operation) {
		this.operation = operation;
	}

	public int getOperation() {
		return operation;
	}
}
