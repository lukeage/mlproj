package de.group5.evosnake.game;

public enum Field {

	HEAD,
	TAIL,
	FOOD,
	EMPTY
}
