package de.group5.evosnake.game;

import java.util.Map;
import java.util.Set;

import de.group5.evosnake.agent.Agent;
import de.group5.evosnake.genetics.Generation;

public class EvolutionController {

	private static final int MAX_TICKS_TO_FOOD = 500;

	public void scoreGeneration(Generation generation) {
		Map<Agent, Integer> scoredAgents = generation.getScoredAgents();
		Set<Map.Entry<Agent, Integer>> entries = scoredAgents.entrySet();
		for (Map.Entry<Agent, Integer> entry : entries) {
			Integer score = scoreAgent(entry.getKey());
			entry.setValue(score);
		}
	}

	private Integer scoreAgent(Agent agent) {
		SnakeController snakeController = new SnakeController(agent);
		int ticksSinceLastFood = 0;
		while (ticksSinceLastFood < MAX_TICKS_TO_FOOD) {
			try {
				snakeController.nextTick();
			} catch (GameOverException e) {
				// System.out.println("Snake died with score " + e.getFinalScore());
				return e.getFinalScore();
			}

			if (snakeController.foundFoodThisTick()) {
				ticksSinceLastFood = 0;
			}
			ticksSinceLastFood++;
		}
		return snakeController.getFinalScore();
	}
}
