package de.group5.evosnake.game;

public class GameState {

	private final Field[][] board;

	private AbsoluteDirection previousDirection;

	public GameState(Field[][] board, AbsoluteDirection previousDirection) {
		this.board = board;
		this.previousDirection = previousDirection;
	}

	public Field[][] getBoard() {
		return board;
	}

	public AbsoluteDirection getPreviousDirection() {
		return previousDirection;
	}

	public void setPreviousDirection(AbsoluteDirection previousDirection) {
		this.previousDirection = previousDirection;
	}
}
