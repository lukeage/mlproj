package de.group5.evosnake;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Set;

import de.group5.evosnake.game.Field;
import de.group5.evosnake.game.Position;
import javax.swing.*;

import static de.group5.evosnake.game.SnakeController.BOARD_HEIGHT;
import static de.group5.evosnake.game.SnakeController.BOARD_WIDTH;

public class BoardUI extends JComponent {

	private static final int GRIDSIZE = 15;

	private final BufferedImage imageBuffer;

	public BoardUI(Field[][] board) {
		this.imageBuffer = new BufferedImage(BOARD_WIDTH * GRIDSIZE, BOARD_HEIGHT * GRIDSIZE,
				BufferedImage.TYPE_INT_RGB);
		initImage(board);
	}

	private void initImage(Field[][] board) {
		Graphics graphics = imageBuffer.getGraphics();
		for (int x = 0; x < BOARD_HEIGHT; x++) {
			for (int y = 0; y < BOARD_WIDTH; y++) {
				drawPositionWithColor(graphics, new Position(x, y), getColor(board[x][y]));
			}

		}
	}

	@Override
	public void paintComponent(Graphics graphics) {
		graphics.drawImage(imageBuffer, 0, 0, imageBuffer.getWidth(), imageBuffer.getHeight(), this);
	}

	public void paintNextTick(Field[][] board, Set<Position> changedPositions) {
		Graphics graphics = imageBuffer.getGraphics();
		for (Position position : changedPositions) {
			Field field = board[position.getX()][position.getY()];
			Color color = getColor(field);
			drawPositionWithColor(graphics, position, color);
		}
	}

	private Color getColor(Field field) {
		Color color;
		switch (field) {
		case EMPTY:
			color = Color.BLACK;
			break;
		case HEAD:
			color = Color.CYAN;
			break;
		case TAIL:
			color = Color.BLUE;
			break;
		case FOOD:
			color = Color.RED;
			break;
		default:
			color = Color.BLACK;
		}
		return color;
	}

	private void drawPositionWithColor(Graphics graphics, Position position, Color color) {
		int xInGrid = position.getX() * GRIDSIZE;
		int yInGrid = position.getY() * GRIDSIZE;

		graphics.setColor(color);
		graphics.fillRect(xInGrid, yInGrid, GRIDSIZE, GRIDSIZE);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(xInGrid, yInGrid, GRIDSIZE, GRIDSIZE);
	}
}
