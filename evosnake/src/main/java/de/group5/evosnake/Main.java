package de.group5.evosnake;

import java.io.FileNotFoundException;
import java.io.IOException;

import de.group5.evosnake.game.EvolutionController;
import de.group5.evosnake.genetics.Generation;
import de.group5.evosnake.genetics.GeneticAlgorithmImpl;

public class Main {

	private static final String MODEL_FILENAME = "simeon";
	private static final int NUMBER_OF_GENERATIONS = 800;
	private static final int START_WITH_GENERATION = 0;

	public static void main(String[] args) throws IOException {

		GeneticAlgorithmImpl geneticAlgorithm = new GeneticAlgorithmImpl();
		Generation currentGeneration;
		try {
			currentGeneration = geneticAlgorithm.loadModel(MODEL_FILENAME + "_" + START_WITH_GENERATION);
		} catch (FileNotFoundException e) {
			System.out.println("Could not find model file with name " + MODEL_FILENAME + ", starting training from scratch.");
			currentGeneration = geneticAlgorithm.initializeGeneration();
		}

		EvolutionController evolutionController = new EvolutionController();
		evolutionController.scoreGeneration(currentGeneration);

		// main loop
		int i = 1 + START_WITH_GENERATION;
		long startTime = System.currentTimeMillis();
		long roundTime;
		int maxScore = 0;
		Generation bestGeneration = null;

		while (i <= START_WITH_GENERATION + NUMBER_OF_GENERATIONS) {
			System.out.println("Scoring Generation " + i + "/" + NUMBER_OF_GENERATIONS);
			roundTime = System.currentTimeMillis();

			currentGeneration = geneticAlgorithm.getNextGeneration(currentGeneration);
			evolutionController.scoreGeneration(currentGeneration);
			currentGeneration.printStatistics();

			if (currentGeneration.getMaxScore() > maxScore) {
				maxScore = currentGeneration.getMaxScore();
				bestGeneration = currentGeneration;
			}

			// Save every 100th generation
			if (i % 100 == 0) {
				geneticAlgorithm.saveModel(MODEL_FILENAME + "_" + i, currentGeneration);
			}

			System.out.println("Scoring Generation " + i + " took " + (float) (System.currentTimeMillis() - roundTime) / 1000 + " seconds");
			i++;
		}

		System.out.println("All " + NUMBER_OF_GENERATIONS
				+ " generations took " + (float) (System.currentTimeMillis() - startTime) / 1000 + " seconds");

		geneticAlgorithm.saveModel(MODEL_FILENAME + "_best", bestGeneration);
	}
}
