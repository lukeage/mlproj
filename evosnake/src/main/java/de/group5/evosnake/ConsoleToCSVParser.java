package de.group5.evosnake;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.File;
public class ConsoleToCSVParser {

    private Scanner sc;
    private File out_file;
    private PrintWriter writer;

    public ConsoleToCSVParser(String file_path, String output_path) throws IOException{ //make sure to replace all "." with "," to recognize doubles
        out_file = new File(output_path);
        FileWriter fw = new FileWriter(out_file);
        writer = new PrintWriter(fw);
        sc = new Scanner(new File(file_path));
    }

    public void convertToCsv(){
        int index;
        double avg_value;
        double max_value;
        while(sc.hasNext()){
            max_value = getNextInt();
            avg_value = getNextDouble();
            index = getNextInt();
            getNextDouble();
            sc.next();
            writer.write(index+";"+max_value +";"+avg_value+"\n");
        }
        writer.close();
        sc.close();
    }

    public static void main(String[] args) throws IOException{
        new ConsoleToCSVParser("3300.txt","1000.csv").convertToCsv();
    }

    public int getNextInt(){
        while(!sc.hasNextInt()){
            sc.next();
        }
        return sc.nextInt();
    }
    public double getNextDouble(){
        while(!sc.hasNextDouble()){
            sc.next();
        }
        return sc.nextDouble();
    }

}
