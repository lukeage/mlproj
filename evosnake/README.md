# EvoSnake

EvoSnake uses Genetic Algorithms to evolve neural networks which can play the game Snake.

## Building

Run `mvn clean install` in this folder to build the project.

## Running

- Run the `Main` class to start training. Specify a model filename and the number of generations you want to train in the 
constants declared in the top of the class. Every 100th generation as well as the best generation will be saved to a 
file in `data/models/`.
- Run the `BestAgentPlayUI` to view the best agents of up to 4 generations. Specify the model filename with 
`MODEL_FILENAME`, and the generations in the call of the method `createBoard("800", frame, snakeControllers, boardUIs);`
with the first parameter. The generation must correspond to a saved generation.

## Hyperparameters/Architecture
Use the `GeneticAlgorithmImpl` class to modify various hyperparameters for training or to set your own network architecture.
The following hyperparameters can be changed:

- `GENERATION_LENGTH`: number of agents in each generation

- `MUTATION_RATE`: probability of a random weight to be affected by mutation

- `MUTATION_RANGE`: applied mutations are in range -MUTATION_RANGE <= x <= MUTATION_RANGE

- `LEARNING_RATE_THRESHOLDS`: specifies average score thresholds where the MUTATION_RANGE is modified by a constant factor -> these are specified in the getNextGeneration method

The neural network archticture is set in the getNewNetwork method:
```java
network = new NeuralNetwork(5);
		network.addLayer(8, ActivationFunction.RELU);
		network.addLayer(3, ActivationFunction.SOFTMAX);
```
First a neural network needs to be created taking the number of input features as a parameter. Currently we are providing 5 features to our network these can be modified in the `transformBoard` method of the `NeuralNetworkAgent` class. 
Now additional hidden layers can be added by calling `.addLayer()` and passing the desired number of neurons and the activation function (`SIGMOID`,`RELU`,`SOFTMAX`). 
The last (output) layer needs to have 3 neurons as we are using these 3 outputs to decide on the next agent move (turn left, turn right or keep straight).
